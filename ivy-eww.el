;;; ivy-eww.el -- helm-eww inspired ivy interface for ivy

;; Package-Requires: ((emacs "26.1") (ivy "0.13.0") (seq "1.8"))
;; TODO -- lower required version numbers

;;; Commentary:
;; Ivy for EWW.
;; Acknowledgement. This package is heavily inspired by helm-eww.el,
;; available at https://github.com/emacs-helm/helm-eww
;;

;;; Code:

(require 'seq)
(require 'ivy)
(require 'eww)

(defun ivy-eww--open-in-new-buffer (url)
  "Opens new eww buffer with URL given as an argument."
  (let ((buff (generate-new-buffer "*eww*")) (old-buff (current-buffer)))
    (set-buffer buff)
    (eww-mode)
    (eww-browse-url url)
    (set-buffer old-buff)))

(defun ivy-eww--list-eww-buffers ()
  "List all buffers with 'eww-mode' enabled."
  (mapcar (lambda (x) (list (with-current-buffer x (plist-get eww-data :title)) x))
  (seq-filter (lambda (x) (with-current-buffer x (derived-mode-p 'eww-mode))) (buffer-list))))



(defun ivy-eww ()
  "Go to eww bookmark or url."
  (interactive)
  (eww-bookmark-prepare)
  (ivy-read "eww url: " (cons (list (ivy-thing-at-point) (ivy-thing-at-point))
			      (append
			       (ivy-eww--list-eww-buffers)
			       (mapcar (lambda (x) (list (plist-get x :title)  'bookmark (plist-get x :url))) eww-bookmarks)))
			      
	    :action (lambda (x)
		      (if (stringp x)
			  (ivy-eww--open-in-new-buffer x)
			(let ((type (car (cdr x))) (val (car (cdr (cdr x)))));; TODO this can be simplified
			  (if (bufferp type) (switch-to-buffer type)
			   (if (or (stringp type) (equal type 'bookmark)) (ivy-eww--open-in-new-buffer val) (error "not implemented"))))))
	    :caller 'counsel-eww
	    ))


(provide 'ivy-eww)
;;; ivy-eww.el ends here
 
